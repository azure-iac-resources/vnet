# Generic Input Variables
# Business Division
variable "business_divsion" {
  description = "Business Division in the large organization this Infrastructure belongs"
  type = string
  default = "gitlab"
}
# Environment Variable
variable "environment" {
  description = "Environment Variable used as a prefix"
  type = string
  default = "dev"
}

# Azure Resource Group Name 
variable "resource_group_name" {
  description = "Resource Group Name"
  type = string
  default = "Ranjith_Practice_2023"  
}

variable "name" {
  type = string
  default = ""
}

# Azure Vnet Address space 
variable "address_space" {
  description = "Vnet Address space"
  type = string
  default = "10.0.0.0/16"
}