module "azurerm_vnet" {
    source = "git::https://gitlab.com/azure-iac2/root-modules.git//vnet"
    #source = "git::https://gitlab.com/azure-iac2/vnet.git//terraform"
    # source = "./modules/"
    business_divsion    = var.business_divsion
    environment         = var.environment
    name                = var.name
    address_space       = var.address_space
    resource_group_name = data.azurerm_resource_group.rg.name
    location            = data.azurerm_resource_group.rg.location
}